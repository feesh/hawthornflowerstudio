<?php

  namespace app\services;

  use app\services\setup\Strip;
  use app\services\setup\Theme;
  use app\services\setup\Bladerunner;
  use app\services\setup\Assets;
  use app\services\setup\ACF;
  use app\models\Register;

  use app\services\Form;

  class Strt
  {
    private $env;

    public function __construct ()
    {
      new Strip;
      new Theme;
      new Bladerunner;
      new Register;
      new Assets;
      new ACF;

      new Form;
    }
  }
