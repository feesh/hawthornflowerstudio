<?php

  namespace app\services\setup;

  use app\services\Environment;

  class Theme
  {
    private $env;

    public function __construct ()
    {
      $this->env = new Environment;
      $this->editor_menu_priviledges();


      add_action('admin_init', [$this, 'admin_init']);
      add_action('after_setup_theme', [$this, 'after_setup_theme']);
      add_action('wp_head', [$this, 'wp_head']);

      // Preservers ACF Serial Number
      add_filter( 'wpmdb_preserved_options', [$this, 'wpmdb_preserved_options']);

      // Sets site as public/private
      add_action('init', [$this, 'init']);

      add_action( 'template_redirect', [$this, 'ssl'], 1 );
    }

    private function editor_menu_priviledges ()
    {
      // get the the role object
      $role_object = get_role( 'editor' );
      // add $cap capability to this role object
      $role_object->add_cap( 'edit_theme_options' );
    }

    public function init ()
    {
      if(ENV_TYPE == 'staging' && get_option('blog_public') == '1') {
        update_option('blog_public', '0');
      }

      if(ENV_TYPE == 'production' && get_option('blog_public') == '0'){
        update_option('blog_public', '1');
      }
    }

    public function admin_init ()
    {
      global $wp_rewrite;
      $pattern = '/%postname%/';
      $wp_rewrite->set_permalink_structure($pattern);
    }

    public function after_setup_theme ()
    {
      // Show the admin bar.
      show_admin_bar(false);

      // Add post thumbnails support.
      add_theme_support('post-thumbnails');

      // Add title tag theme support.
      add_theme_support('title-tag');

      // Add HTML5 support.
      add_theme_support('html5', [
        'caption',
        'comment-form',
        'comment-list',
        'gallery',
        'search-form',
        'widgets',
      ]);

      add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );

      add_filter('max_srcset_image_width', function () {return 1;});
    }

    public function wp_head ()
    {
?>
      <link rel="icon shortcut" href="<?= $this->env->tmplt() ?>/assets/images/favicon.png " />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta charset="utf-8">
<?php
    }

    public function wpmdb_preserved_options ($options)
    {
      $options[] = 'acf_pro_license';
      return $options;
    }

    public function ssl ()
    {
      if ( isset($_ENV['FORCE_SSL']) && $_ENV['FORCE_SSL'] && !is_ssl() && !is_admin() ) {

  			if ( 1 === strpos( $_SERVER['REQUEST_URI'], 'http' ) ) {
  				wp_redirect( preg_replace( '|^http://|', 'https://', $_SERVER['REQUEST_URI'] ), 301 );

          exit;
  			} else {

  				wp_redirect( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 301 );
  				exit();
  			}

  		}
    }
  }
