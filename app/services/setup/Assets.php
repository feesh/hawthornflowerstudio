<?php

  namespace app\services\setup;

  use app\services\Environment;
  use app\services\Helpers;

  class Assets
  {
    private $env;
    private $hlp;

    function __construct ()
    {
      $this->hlp = new Helpers;
      $this->env = new Environment;
      add_action('wp_enqueue_scripts', [$this, 'enqueue']);
    }

    public function enqueue ()
    {
      $this->styles();
      $this->scripts();
    }

    private function styles ()
    {
      $path = $this->env->tmplt() . '/';

      wp_register_style('font-apercu', $path . 'assets/fonts/apercu.css');
      wp_register_style('typography', 'https://cloud.typography.com/6538294/7349012/css/fonts.css');
      wp_register_style('slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css');
      wp_register_style('app', $this->hlp->mix('/css/app.css'), ['font-apercu', 'typography', 'slick']);
      wp_enqueue_style('app');
    }

    private function scripts ()
    {
      wp_deregister_script('jquery');
      wp_deregister_script( 'wp-embed' );
      wp_register_script('jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js', '3.3.1', false, true);
      wp_register_script('slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js', '1.9.0', false, true);
      wp_register_script('app', $this->hlp->mix('/js/app.js'), ['jquery', 'slick'], false, true);
      wp_enqueue_script('app');

      # localize
      wp_localize_script('app', 'STRT', ['ajax' => admin_url('admin-ajax.php')]);
    }
  }
