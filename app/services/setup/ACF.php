<?php

  namespace app\services\setup;

  class ACF
  {

    function __construct ()
    {
      if (function_exists('acf_add_options_page')) {
        acf_add_options_page('Site Options');
      }
    }
  }
