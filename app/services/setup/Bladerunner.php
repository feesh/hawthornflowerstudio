<?php

  namespace app\services\setup;

  class Bladerunner
  {

    function __construct ()
    {
      add_filter('bladerunner/cache/make', function() {
        return false;
      });

      add_filter('bladerunner/template/bladepath', function($paths) {
        if(!is_array($paths)) $paths = [$paths];
        $paths[] = APP_ROOT . "/app";
        return $paths;
      });

      add_filter('bladerunner/controller/paths', function ($paths) {
        $paths[] = get_template_directory() . '/app/controllers';
        $paths[] = APP_ROOT . '/app/controllers';
        return $paths;
      });
    }
  }
