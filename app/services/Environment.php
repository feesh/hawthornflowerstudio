<?php

    namespace app\services;

    class Environment
    {
      private $tmplt;
      private $tmpltd;

      function __construct ()
      {
        $this->tmplt = get_bloginfo('template_directory');
        $this->tmpltd = get_template_directory();
      }

      public function tmplt ()
      {
        return $this->tmplt;
      }

      public function tmpltd ()
      {
        return $this->tmpltd;
      }
    }
