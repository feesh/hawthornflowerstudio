<?php

  namespace app\services;

  use app\services\Environment;

  class Helpers
  {
    private $env;

    function __construct ()
    {
      $this->env = new Environment();
    }

    public function mix($path)
    {
      $manifest = $this->env->tmpltd().'/assets/mix-manifest.json';
      $manifest = json_decode(file_get_contents($manifest), true);

      return $this->env->tmplt().'/assets'.$manifest[$path];
    }

    public function pull_in_svg($filename)
    {
      return file_get_contents($this->env->tmpltd().'/assets/images/'.$filename.'.svg');
    }
  }
