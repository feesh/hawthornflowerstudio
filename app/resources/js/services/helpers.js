function scrollTo ({$element, offset}) {
    $('html, body').animate({scrollTop: $element.offset().top - offset}, 500);
}

export {scrollTo}
