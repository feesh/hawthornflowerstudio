import credit from './modules/credit'
import form from './modules/form'
import gallery from './modules/gallery'
import load from './modules/load'
import menu from './modules/menu'
import scroll from './modules/scroll'

$(() => {
  new credit
  new gallery
  new load
  new menu
  new scroll
  new form($('form'))
})
