import cache from '../services/cache'

export default class Gallery {
  constructor() {
    this.$el = $('[modal\\@gallery]')
    this.$slides = this.$el.find('[modal\\@gallery\\:slides]')
    this.$images = $('[gallery\\@image]')
    this.$close = $('[modal\\@gallery\\:close]')

    this.$prev = $('[modal\\@gallery\\:previous]')
    this.$next = $('[modal\\@gallery\\:next]')

    this.built = false

    this.handle()
  }

  handle () {
    this.$images.on('click', (event) => {
      event.preventDefault()

      if (!this.built) {
        this.buildSlider()
      }

      const index = this.$images.index($(event.currentTarget))
      this.$slides.slick('slickGoTo', index)

      cache.$body.addClass('state:modal@gallery::visible')
    })

    this.$close.on('click', (event) => {
      event.preventDefault()
      cache.$body.removeClass('state:modal@gallery::visible')
    })

    this.$prev.on('click', (event) => {
      event.preventDefault()
      this.$slides.slick('slickPrev')
    })

    this.$next.on('click', (event) => {
      event.preventDefault()
      this.$slides.slick('slickNext')
    })
  }

  setup () {

  }

  buildSlider () {
    const slides = this.buildSlides()
    this.$slides.html(slides)

    this.$slides.slick({
      arrows: false
    })

    this.built = true
  }

  buildSlides () {
    let html = ''

    this.$images.each((index, item) => {
      const img = $(item).find('img')
      html += `<div class="component:modal@gallery::slide">${img.get(0).outerHTML}</div>`
    })

    return html
  }
}
