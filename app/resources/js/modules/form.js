import cache from '../services/cache'

function validateEmail(email)
{
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
}

export default class Form {
  constructor($form) {
    this.$el = $form
    this.$button = this.$el.find('button[type="submit"]')
    this.$close = $('.form\\:inquire\\:\\:close')
    this.handle()
  }

  handle () {
    this.$el.on('submit', (event) => {
      event.preventDefault()
      this.submit()
    })

    this.$close.on('click', (event) => {
      event.preventDefault()
      cache.$body.removeClass('state:form::success')
    })
  }

  process () {
    const url = STRT.ajax
    const data = this.$el.serialize()
    return $.post(url, data)
  }

  processing () {
    this.$button.text('sending').attr('disabled', 'disabled')
    this.$el.addClass('»processing')
  }

  processed () {
    this.$el.removeClass('»processing')
    this.$button.text('send').removeAttr('disabled')
  }

  isValid () {
    let valid = false

    const $inputs = this.$el.find('[data-required]')

    $inputs.each((index, element) => {
      const $input = $(element)
      const value = $input.val()
      const type = $input.attr('type')

      if (!value) {
        $input.addClass('has-error')
      }

      if (value) {
        $input.removeClass('has-error')
      }

      if (value && type === 'email') {
        if (!validateEmail(value)) {
          $input.addClass('has-error')
        } else {
          $input.removeClass('has-error')
        }
      }
    })

    return !this.$el.find('.has-error').length
  }

  submit () {
    if (this.isValid()) {
      const request = this.process()

      this.processing()

      request.done((response) => {
        this.success(response)
      }).fail((response) => {
        this.fail(response)
      }).always(() => {
        this.processed()
      })
    }
  }

  success (response) {
    if (response.status === 'success') {
      cache.$body.addClass('state:form::success')
      this.$el[0].reset()

      setTimeout(() => {
        cache.$body.removeClass('state:form::success')
      }, 10000);
    }
  }

  fail (response) {}
}
