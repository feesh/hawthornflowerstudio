import {scrollTo} from '../services/helpers'

export default class Scroll {
  constructor() {
    this.$el = $('[scroll]')
    this.handle()
  }

  handle () {
    this.$el.on('click', (event) => {
      event.preventDefault()
      const $this = $(event.currentTarget)
      const target = $this.attr('href')
      const $element = $(target)
      const offset = 0

      scrollTo({
        $element,
        offset
      })
    })
  }
}
