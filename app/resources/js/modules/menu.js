import cache from '../services/cache'

export default class Menu {
  constructor() {
    this.$el = $('[menu]')
    this.handle()
  }

  handle () {
    this.$el.on('click', () => {
      cache.$body.toggleClass('state:menu::open')
    })
  }
}
