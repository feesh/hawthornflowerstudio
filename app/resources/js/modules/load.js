export default class Load {
  constructor() {
    this.$el = $('[load]')
    this.$loader = $('#loader')
    this.page = parseInt(this.$el.attr('load'))
    this.max = parseInt(this.$el.attr('max'))

    console.log(this.page, this.max)

    this.handle()
  }

  handle () {
    this.$el.on('click', (event) => {
      event.preventDefault()

      if (this.page <= this.max) {
        const response = $.get(window.location, {
          page: this.page
        })

        response.done((html) => {
          this.$loader.append($(html).find('#loader').html())
          this.page++

          if (this.page >= this.max) {
            this.$el.next('span').remove()
            this.$el.remove()
          }
        })
      }
    })
  }
}
