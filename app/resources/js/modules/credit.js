export default class Credit {
  constructor() {
    this.$el = $('[credits]')
    this.$boxed = $('[credits\\@boxed]')
    this.handle()
  }

  handle () {
    this.$el.on('click', (event) => {
      event.preventDefault()
      this.$boxed.slideToggle(250)
    })
  }
}
