<div class="component:gallery">
  @foreach ($gallery as $row)
    @include('views.components.gallery-'. $row['acf_fc_layout'], $row)
  @endforeach
</div>
