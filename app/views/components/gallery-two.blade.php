@php
  $size = ($type == 'horizontal') ? [585*2, 350*2] : [585*2, 790*2];
@endphp

<div class="component:gallery::row »two">
  <div class="component:gallery::box">
    <a href="#" gallery@image>{!! wp_get_attachment_image($image_left, $size) !!}</a>
  </div>
  <div class="component:gallery::box">
    <a href="#" gallery@image>{!! wp_get_attachment_image($image_right, $size) !!}</a>
  </div>
</div>
