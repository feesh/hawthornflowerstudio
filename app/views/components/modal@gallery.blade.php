<aside class="component:modal@gallery" modal@gallery>
  <button class="component:modal@gallery::close component:button@menu »close" modal@gallery:close>
    <div class="component:button@menu::line"></div>
    <div class="component:button@menu::line"></div>
    <div class="component:button@menu::line"></div>
  </button>
  <div class="component:modal@gallery::box">
    <a href="#" class="component:modal@gallery::arrow »previous" modal@gallery:previous>• Prev</a>
    <a href="#" class="component:modal@gallery::arrow »next" modal@gallery:next>Next •</a>
    <div class="component:modal@gallery::slides" modal@gallery:slides></div>
  </div>
</aside>
