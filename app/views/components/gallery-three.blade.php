@php
  $width = 405 * 2;
  $height = 500 * 2;
@endphp

<div class="component:gallery::row »three">
  <div class="component:gallery::box">
    <a href="#" gallery@image>{!! wp_get_attachment_image($image_one, [$width, $height]) !!}</a>
  </div>
  <div class="component:gallery::box">
    <a href="#" gallery@image>{!! wp_get_attachment_image($image_two, [$width, $height]) !!}</a>
  </div>
  <div class="component:gallery::box">
    <a href="#" gallery@image>{!! wp_get_attachment_image($image_three, [$width, $height]) !!}</a>
  </div>
</div>
