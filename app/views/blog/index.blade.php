@extends('views.layouts.master')
@php
  $last = 0;
@endphp

@section('content')
  <main class="view view:blogs">
    <div id="loader" class="block:blogs">
      <div class="block:blogs::row">
        @foreach ($items as $item)
          @if ($loop->index % 3 !== 0)
            <div class="block:blogs::spacer"></div>
          @endif
          <div class="block:blogs::box">
            @include('views.cards.blog', $item)
          </div>
          @php
            $last = $loop->index;
          @endphp
        @endforeach
        @for ($i = 0; $i < $remainder; $i++)
          @if (($last + $loop->index) % 3 !== 0)
            <div class="block:blogs::spacer »empty"></div>
          @endif
          <div class="block:blogs::box »empty"></div>
        @endfor
      </div>
    </div>
    <aside class="block:next">
      @if ($next)
        <a href="#hfs" max="{{ $max }}" load="{{ $next }}">Load More</a> <span>•</span>
      @endif
      <a href="#hfs" scroll>Back To Top</a>
    </aside>
  </main>
@endsection
