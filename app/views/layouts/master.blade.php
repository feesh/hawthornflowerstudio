<!DOCTYPE html>
<html>
  <head>
    @php wp_head(); @endphp
  </head>
  <body id="hfs" class="{{ $class }}">
    @if (!$splash)
      @include('views.blocks.navigation')
    @endif
    <header class="block:header">
      <div class="block:header::boxes">
        <div class="block:header::box"></div>
        <div class="block:header::box">
          <h1 class="block:header::logo">
            <a href="/">{!! $svg['logo'] !!}</a>
          </h1>
        </div>
        <div class="block:header::box">
          @if (!$splash)
            <button class="component:button@menu" menu>
              <div class="component:button@menu::line"></div>
              <div class="component:button@menu::line"></div>
              <div class="component:button@menu::line"></div>
            </button>
          @endif
        </div>
      </div>
    </header>
    @yield('content')
    @if ($splash)
      @include('views.blocks.footer-splash')
    @else
      @include('views.blocks.footer')
    @endif
    @php wp_footer(); @endphp
  </body>
</html>
