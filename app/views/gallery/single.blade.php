@extends('views.layouts.master')

@section('content')
  <main class="view view:gallery@single">
    <article class="block:gallery">
      <div class="component:intro">
        <div class="component:intro::box">
          <a href="#" gallery@image>{!! wp_get_attachment_image($image, [820, 0]) !!}</a>
        </div>
        <div class="component:intro::box">
          <h2 class="component:intro::heading">
            {!! $heading !!}
          </h2>
          <div class="component:intro::text">
            {!! $content !!}
            {!! $details !!}
          </div>
          <nav class="component:intro::navigation">
            @if ($previous)
              <a href="{{ $previous }}">Prev</a> •
            @endif
            <a href="{{ $navigation['gallery'] }}">Main Gallery</a>
            @if ($next)
               • <a href="{{ $next }}">Next</a>
            @endif
          </nav>
        </div>
      </div>
      @if ($gallery)
        @include('views.components.gallery', $gallery)
      @endif
    </article>
    <aside class="block:next">
      <a href="#hfs" scroll>Back To Top</a>
    </aside>
    <aside class="block:next">
      @if ($previous)
        <a href="{{ $previous }}">Prev</a> •
      @endif
      <a href="{{ $navigation['gallery'] }}">Main Gallery</a>
      @if ($next)
         • <a href="{{ $next }}">Next</a>
      @endif
    </aside>
  </main>
  @include('views.components.modal@gallery')
@endsection
