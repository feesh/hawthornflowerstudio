@extends('views.layouts.master')

@section('content')
  <main class="view:galleries">
    <div class="block:galleries">
      <div id="loader" class="block:galleries::boxed">
        @foreach ($items as $item)
          @include('views.cards.gallery', $item)
        @endforeach
      </div>
    </div>
    <aside class="block:next">
      @if ($next)
        <a href="#hfs" max="{{ $max }}" load="{{ $next }}">Load More</a> <span>•</span>
      @endif
      <a href="#hfs" scroll>Back To Top</a>
    </aside>
    <aside class="block:next">
      <a href="{{ $navigation['work'] }}">
        <em>View</em>
        <mark>H.F.S Corporate Work</mark>
      </a>
    </aside>
  </main>
@endsection
