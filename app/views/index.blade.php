@extends('views.layouts.master')

@section('content')
  <main class="view:home">
    <div class="block:home">
      <div class="block:home::row »dual">
        <div class="block:home::box »half">
          @include('views.cards.featured', $links[0])
        </div>
        <div class="block:home::box »half">
          <a href="#" class="card:featured">
            @include('views.cards.featured', $links[1])
          </a>
        </div>
      </div>
      <div class="block:home::row">
        <div class="block:home::box »full">
          @include('views.cards.featured', array_merge(
            $links[2],
            ['type' => 'full']
          ))
        </div>
      </div>
    </div>
  </main>
@endsection
