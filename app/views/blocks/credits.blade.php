<div class="block:credits">
  <p><a href="#" credits>Site Credits</a></p>
  <div class="block:credits::boxed" credits@boxed>
    @foreach ($credits as $credit)
      <div class="block:credits::box">
        <h4 class="block:credits::heading">{{ $credit['heading'] }}</h4>
        <nav class="block:credits::navigation">
          @foreach ($credit['links'] as $item)
            @php
              $link = $item['link']
            @endphp
            <a href="{{ $link['url'] }}" target="{{ $link['target'] }}">{{ $link['title'] }}</a>
          @endforeach
        </nav>
      </div>
    @endforeach
  </div>
</div>
