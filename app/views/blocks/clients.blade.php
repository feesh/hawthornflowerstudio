<div class="block:clients">
  <h3 class="block:clients::heading">Select Clients</h3>
  <h4 class="block:clients::subheading">Past and Present</h4>
  <ul class="block:clients::list">
    @foreach ($clients as $client)
      @include('views.cards.client', $client)
    @endforeach    
  </ul>
</div>
