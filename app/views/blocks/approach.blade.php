<div class="block:our@approach">
  <div class="block:our@approach::box">
    <figure class="block:our@story::image">
      <picture>
        @php
          $small = wp_get_attachment_image_src($image, [375, 505])
        @endphp
        <source srcset="{{ $small[0] }}" media="(max-width: 780px)" />
        {!! wp_get_attachment_image($image, [890, 1235]) !!}
      </picture>
    </figure>
  </div>
  <div class="block:our@approach::box">
    <h2 class="block:our@story::heading">{{ $heading }}</h2>
    <div class="block:our@story::content">
      {!! $content !!}
    </div>
  </div>
</div>
