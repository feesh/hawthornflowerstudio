<footer class="block:footer »splash">
  <div class="block:footer::boxed">
    <div class="block:footer::boxes">
      <div class="block:footer::box">
        <p>Hawthorn Flower Studio is devoted to creating beautiful floral designs for weddings and events in Northern California.</p>
      </div>
      <div class="block:footer::box">
        <p>New Site Coming Soon</p>
        <p class="block:footer::copyright">&copy;{{ Date('Y') }} Hawthorn Flower Studio</p>
      </div>
      <div class="block:footer::box">
        <nav class="block:footer::social component:social">
          <a href="{{ $social['instagram'] }}" target="_blank" class="component:social::instagram">{!! $svg['icon-instagram'] !!}</a>
          <a href="{{ $social['pinterest'] }}" target="_blank" class="component:social::pinterest">{!! $svg['icon-pinterest'] !!}</a>
        </nav>
        <p><a href="mailto:hello@hawthornflowerstudio.com">hello@hawthornflowerstudio.com</a></p>
      </div>
    </div>
  </div>
</footer>
