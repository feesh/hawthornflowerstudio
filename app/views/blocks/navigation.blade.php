<aside class="block:navigation">
  <div class="block:navigation::boxed">
    <nav class="block:navigation::nav component:navigation">
      <a href="{{ $navigation['our-story'] }}">Our Story</a>
      <a href="{{ $navigation['our-services'] }}">Our Services</a>
      <a href="{{ $navigation['gallery'] }}">Gallery</a>
      <a href="{{ $navigation['contact'] }}">Contact</a>
      <a href="{{ $navigation['blog'] }}">Journal</a>
    </nav>
    <div class="block:navigation::boxes">
      <div class="block:navigation::box">
        <p>Hawthorn Flower Studio is devoted to creating beautiful floral designs for weddings and events in Northern California.</p>
      </div>
      <div class="block:navigation::box">
        <nav class="block:navigation::social component:social">
          <a href="{{ $social['instagram'] }}" target="_blank" class="component:social::instagram">{!! $svg['icon-instagram'] !!}</a>
          <a href="{{ $social['pinterest'] }}" target="_blank" class="component:social::pinterest">{!! $svg['icon-pinterest'] !!}</a>
        </nav>
        <p><a href="mailto:hello@hawthornflowerstudio.com">hello@hawthornflowerstudio.com</a></p>
      </div>
    </div>
  </div>
</aside>
