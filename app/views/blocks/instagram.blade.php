<aside class="block:instagram">
  <h4 class="block:instagram::heading">Follow</h4>
  <div class="block:instagram::action"><a href="{{ $social['instagram'] }}" target="_blank">@hawthornflowerstudio</a></div >
  <div class="block:instagram::boxes">
    @foreach ($instagram as $image)
      <div class="block:instagram::box">
        <a href="{{ $image['url'] }}" target="_blank"><img src="{{ $image['image'] }}"></a>
      </div>
    @endforeach
  </div>
</aside>
