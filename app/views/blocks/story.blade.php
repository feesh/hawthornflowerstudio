<figure class="block:our@story::image">
  {!! wp_get_attachment_image($image, [375, 505]) !!}
</figure>
<h3 class="block:our@story::heading">{{ $heading }}</h3>
<div class="block:our@story::content">
  {!! $content !!}
</div>
