<footer class="block:footer">
  <div class="block:footer::boxed">
    <figure class="block:footer::mark">
      <a href="/">{!! $svg['mark'] !!}</a>
    </figure>
    <div class="block:footer::boxes">
      <div class="block:footer::box">
        <p>Hawthorn Flower Studio is devoted to creating beautiful floral designs for weddings and events in Northern California.</p>
      </div>
      <div class="block:footer::box">
        <p class="block:footer::copyright">&copy; {{ Date('Y') }} Hawthorn Flower Studio</p>
        <div class="hide:mobile">
          @include('views.blocks.credits')
        </div>
      </div>
      <div class="block:footer::box">
        <nav class="block:footer::social component:social">
          <a href="{{ $social['instagram'] }}" target="_blank" class="component:social::instagram">{!! $svg['icon-instagram'] !!}</a>
          <a href="{{ $social['pinterest'] }}" target="_blank" class="component:social::pinterest">{!! $svg['icon-pinterest'] !!}</a>
        </nav>
        <p><a href="mailto:hello@hawthornflowerstudio.com">hello@hawthornflowerstudio.com</a></p>
      </div>
      <div class="block:footer::box »alt">
        @include('views.blocks.credits')
      </div>
    </div>
  </div>
</footer>
