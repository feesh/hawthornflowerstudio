<li>
  @if ($link)
    <a href="{{ $link }}" target="_blank">
  @endif
  @if ($is_print)
    <em>
  @endif
    {{ $heading }}
  @if ($is_print)
  </em> (print)
  @endif
  @if ($link)
    </a>
  @endif
</li>
