<li>
  @if ($link)
    <a href="{{ $link }}" target="_blank">
  @endif
    {{ $heading}}
  @if ($link)
    </a>
  @endif
</li>
