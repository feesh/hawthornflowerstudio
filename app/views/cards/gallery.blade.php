@php
  if ($imagemeta['width'] > $imagemeta['height']) {
    $type = 'one';
  }

  if ($imagemeta['height'] > $imagemeta['width']) {
    $type = 'two';
  }
@endphp


<div class="block:galleries::box »{{ $type }}">
  <a href="{{ $link }}" class="card:gallery">
    <figure class="card:gallery::image">
      @if ($type === 'one')
        @php
          $small = wp_get_attachment_image_src($image, [370, 540])
        @endphp
        <picture>
          <source srcset="{{ $small[0] }}" media="(max-width: 780px)" />
          {!! wp_get_attachment_image($image, [1180, 800]) !!}
        </picture>
      @endif

      @if ($type === 'two')
        {!! wp_get_attachment_image($image, [370, 540]) !!}
      @endif
      <figcaption class="card:gallery::caption">
        <h3 class="card:gallery::heading">{{ $heading }}</h3>
        <div class="card:gallery::text">{{ $location }}</div>
      </figcaption>
    </figure>
  </a>
</div>
