<article class="card:blog">
  <a href="{{ $link }}">
    <figure class="card:blog::image">
      {!! wp_get_attachment_image($image, [350, 475]) !!}
      <figcaption class="card:blog::caption">
        <h2 class="card:blog::heading">{{ $heading }}</h2>
        <time class="card:blog::time">October 2018</time>
      </figcaption>
    </figure>
  </a>
</article>
