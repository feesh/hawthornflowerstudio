<a href="{{ $link }}" class="card:featured">
  <figure class="card:featured::image">
    @if ($type === 'full')
      @php
        $small = wp_get_attachment_image_src($image, [580, 875])
      @endphp
      <picture>
        <source srcset="{{ $small[0] }}" media="(max-width: 780px)" />
        {!! wp_get_attachment_image($image, [1205, 805]) !!}
      </picture>
    @else
      {!! wp_get_attachment_image($image, [580, 875]) !!}
    @endif
    <figcaption class="card:featured::caption">
      <em>{{ $subheading }}</em>
      {{ $heading }}
    </figcaption>
  </figure>
</a>
