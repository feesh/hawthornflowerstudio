@extends('views.layouts.master')

@section('content')
  <main class="view view:splash">
    <div class="block:splash">
      @include('views.blocks.instagram', $instagram)
    </div>
  </main>
@endsection
