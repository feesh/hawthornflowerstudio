@extends('views.layouts.master')

@section('content')
  <main class="view">
    <article class="block:work">
      <div class="component:intro">
        <div class="component:intro::box">
          <a href="#" gallery@image>{!! wp_get_attachment_image($image, [820, 0]) !!}</a>
        </div>
        <div class="component:intro::box">
          <h2 class="component:intro::heading">
            {!! $heading !!}
          </h2>
          <div class="component:intro::text">
            {!! $content !!}
          </div>
          @include('views.blocks.clients', $clients)
          <nav class="component:intro::navigation">
            <a href="{{ $navigation['gallery'] }}">Back to Gallery</a> • <a href="{{ $navigation['our-services'] }}">Back to Services</a>
          </nav>
        </div>
      </div>
      @if ($gallery)
        @include('views.components.gallery', $gallery)
      @endif
    </article>
    <aside class="block:next">
      <a href="{{ $navigation['gallery'] }}">Back to Gallery</a> • <a href="{{ $navigation['our-services'] }}">Back to Services</a>
    </aside>
  </main>
  @include('views.components.modal@gallery')
@endsection
