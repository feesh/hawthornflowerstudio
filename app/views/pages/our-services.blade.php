@extends('views.layouts.master')

@section('content')
  <main class="view:our-services">
    <article class="block:services">
      <figure class="block:services::image">
        {!! wp_get_attachment_image($image, [1200, 880]) !!}
      </figure>
      <div class="block:services::boxes block:services::content">
        <div class="block:services::box">
          {!! $col1 !!}
        </div>
        <div class="block:services::box">
          {!! $col2 !!}
        </div>
        <div class="block:services::box">
          {!! $col3 !!}
        </div>
      </div>
    </article>
    <aside class="block:next">
      <a href="{{ $navigation['work'] }}">
        <em>View</em>
        <mark>H.F.S Corporate Work</mark>
      </a>
    </aside>
    <aside class="block:next »mobile">
      <a href="#hfs" scroll>Back to Top</a>
    </aside>
  </main>
@endsection
