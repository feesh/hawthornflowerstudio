@extends('views.layouts.master')

@section('content')
  <main class="view:our-story">
    <article class="block:our@story">
      @include('views.blocks.approach', $approach)
      <div class="block:our@story::boxes">
        <div class="block:our@story::box">
          @include('views.blocks.story', $owner)
        </div>
        <div class="block:our@story::spacer"></div>
        <div class="block:our@story::box">
          @include('views.blocks.story', $namesake)
        </div>
        <div class="block:our@story::spacer"></div>
        <div class="block:our@story::box">
          <h4 class="block:our@story::heading">As Seen In</h4>
          <ul class="block:our@story::links">
            @foreach ($press as $item)
              @include('views.cards.press', $item)
            @endforeach
          </ul>
        </div>
      </div>
    </article>
    <aside class="block:next">
      <a href="#hfs" scroll>Back to Top</a>
    </aside>
  </main>
@endsection
