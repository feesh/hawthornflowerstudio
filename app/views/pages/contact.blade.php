@extends('views.layouts.master')

@section('content')
  <main class="view:contact">
    <div class="block:contact">
      <div class="block:contact::boxes">
        <div class="block:contact::box">
          <h2 class="block:contact::heading">
            Inquire
            <em>all fields required</em>
          </h2>
          <form action="#" class="form:inquire">
            <div class="form:inquire::row »dual">
              <div class="form:inquire::box">
                <input type="text" class="form:inquire::input" placeholder="Your Name (first and last)" name="data[your_name]" data-required>
              </div>
              <div class="form:inquire::box">
                <input type="text" class="form:inquire::input" placeholder="Your Email Address" name="data[email]" data-required>
              </div>
            </div>
            <div class="form:inquire::row »dual">
              <div class="form:inquire::box">
                <input type="text" class="form:inquire::input" placeholder="Your Partner’s Name" name="data[your_partners_name]" data-required>
              </div>
              <div class="form:inquire::box">
                <input type="text" class="form:inquire::input" placeholder="Event Venue"  name="data[event_venue]" data-required>
              </div>
            </div>
            <div class="form:inquire::row »dual">
              <div class="form:inquire::box">
                <input type="text" class="form:inquire::input" placeholder="Photographer’s Name" name="data[photographer]" data-required>
              </div>
              <div class="form:inquire::box">
                <input type="text" class="form:inquire::input" placeholder="Event Planner’s Name" name="data[event_planner]" data-required>
              </div>
            </div>
            <div class="form:inquire::row">
              <label for="" class="form:inquire::label">Message</label>
              <textarea  name="data[message]" data-required rows="8" cols="80" class="form:inquire::input"></textarea>
            </div>
            <div class="form:inquire::action">
              <button type="submit" class="form:inquire::button component:button">Send</button>
            </div>
            <aside class="form:inquire::success">
              <button class="form:inquire::close component:button@menu »close">
                <div class="component:button@menu::line"></div>
                <div class="component:button@menu::line"></div>
                <div class="component:button@menu::line"></div>
              </button>
              <div class="form:inquire::boxed">
                <h5 class="form:inquire::heading">Message Sent</h5>
                <div class="form:inquire::text">
                    <p>We will respond within 48 hours unless we are away at an event.</p>
                    <p>Thank you!</p>
                </div>
              </div>
            </aside>
            <input type="hidden" name="action" value="process:form">
          </form>
        </div>
        <div class="block:contact::spacer"></div>
        <div class="block:contact::box">
          <h3 class="block:contact::heading">Get In Touch</h3>
          <ul class="block:contact::list">
            <li>
              Email: <a href="mailto:hello@hawthornflowerstudio.com">hello@hawthornflowerstudio.com</a>
            </li>
            <li>
              Phone: 510-364-2299
            </li>
            <li>
              Instagram: <a href="{{ $social['instagram'] }}" target="_blank">@hawthornflowerstudio</a>
            </li>
            <li>
              Studio Location: Oakland, California
            </li>
            <li>
              Service Area: Northern California
            </li>
          </ul>
        </div>
      </div>
      @include('views.blocks.instagram', $instagram)
    </div>
    <aside class="block:next »mobile">
      <a href="#hfs" scroll>Back to Top</a>
    </aside>
  </main>
@endsection
