<?php
  namespace app\models;

  use Fresa\PostModel;
  use Fresa\Taxonomy;
  # https://fresa.jplhomer.org/docs/post-models

  class Gallery extends PostModel
  {

    protected $postType = 'gallery';

    public function categories()
    {
      return $this->hasTaxonomy(GalleryCategory::class);
    }

  }

  class GalleryCategory extends Taxonomy
  {
    public function events() {
      return $this->belongsToPost(Gallery::class);
    }
  }
