<?php

  namespace app\models;

  use app\models\Gallery;

  class Register
  {
    function __construct ()
    {
      // register models here
      Gallery::register([
        'supports' => ['title', 'editor', 'thumbnail']
      ]);

      GalleryCategory::register([Gallery::class], [
        'hierarchical' => true,
        'show_admin_column' => true
      ]);
    }
  }
