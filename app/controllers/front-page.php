<?php
  namespace app\controllers;

  use app\controllers\Base;
  use app\services\Instagram;

  class FrontPage extends Base
  {
    protected $template='views.index';

    public function __construct ()
    {
      parent::__construct();

      // if (!is_user_logged_in()) {
      //   $this->template = 'views.splash';
      //   $this->class = 'template:splash';
      // }
    }

    public function links ()
    {
      return get_field('links', $this->id);
    }

    public function instagram ()
    {
      $instagram = new Instagram(4);
      return $instagram->images();
    }
  }
