<?php
  namespace app\controllers;

  use app\controllers\Base;

  class Index extends Base
  {
    protected $template='views.pages.our-story';

    public function __construct ()
    {
      parent::__construct();
      $this->class = 'template:our-story';
    }

    public function approach ()
    {
      return get_field('our_approach', $this->id);
    }

    public function owner ()
    {
      return get_field('our_owner', $this->id);
    }

    public function namesake ()
    {
      return get_field('our_namesake', $this->id);
    }

    public function press ()
    {
      return get_field('press', $this->id);
    }
  }
