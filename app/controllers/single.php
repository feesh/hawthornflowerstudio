<?php
  namespace app\controllers;

  use app\controllers\Base;

  class BlogSingle extends Base
  {
    protected $template='views.blog.single';

    public function __construct ()
    {
      parent::__construct();
      $this->class = 'template:blog@single';
    }

    public function image ()
    {
      return get_field('featured_image') ?? get_post_thumbnail_id($this->id);
    }

    public function heading ()
    {
      return get_the_title();
    }

    public function content ()
    {
      return apply_filters('the_content', get_post_field('post_content', $this->id));
    }

    public function details ()
    {
      return get_field('project_details', $this->id);
    }

    public function gallery ()
    {
      return get_field('gallery', $this->id);
    }

    public function next ()
    {
      $next = get_previous_post();

      if ($next) {
        return get_permalink($next);
      }
    }

    public function previous ()
    {
      $previous = get_next_post();

      if ($previous) {
        return get_permalink($previous);
      }
    }
  }
