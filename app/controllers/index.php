<?php
  namespace app\controllers;

  use app\controllers\Base;
  use app\models\Post;

  class BlogIndex extends Base
  {
    protected $template='views.blog.index';
    private $limit = 9;
    private $page = 0;
    private $max = null;
    public $items;

    public function __construct ()
    {
      parent::__construct();
      $this->items = $this->get();
      $this->class = 'template:blog@archive';
      $this->max = ceil(count(Post::all())/$this->limit) - 1;
      $this->page = isset($_GET['page']) ? $_GET['page'] : 0;
    }

    private function get ()
    {
      return Post::limit($this->limit)->offset($this->page * $this->limit)->get()->map(function ($item) {
        return [
          'image' => get_post_thumbnail_id($item->id),
          'heading' => $item->title,
          'time' => 'Oct',
          'link' => $item->permalink()
        ];
      });
    }

    public function items ()
    {
      return $this->items;
    }


    public function remainder ()
    {
      $count = count($this->items);
      return 3 - ($count%3);
    }

    public function next ()
    {
      return $this->page < $this->max ? $this->page + 1 : false;
    }

    public function max ()
    {
      return $this->max;
    }
  }
