<?php
  namespace app\controllers;

  use app\controllers\Base;
  use app\services\Instagram;

  class Index extends Base
  {
    protected $template='views.pages.work';

    public function __construct ()
    {
      parent::__construct();
      $this->class = 'template:corporate-work';
    }

    public function image ()
    {
      return get_post_thumbnail_id($this->id);
    }

    public function heading ()
    {
      return get_the_title();
    }

    public function content ()
    {
      return apply_filters('the_content', get_post_field('post_content', $this->id));
    }

    public function gallery ()
    {
      return get_field('gallery', $this->id);
    }

    public function clients ()
    {
      return get_field('clients', $this->id);
    }
  }
