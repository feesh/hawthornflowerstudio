<?php
  namespace app\controllers;

  use app\controllers\Base;
  use app\services\Instagram;

  class Index extends Base
  {
    protected $template='views.pages.contact';

    public function __construct ()
    {
      parent::__construct();
      $this->class = 'template:contact';
    }

    public function instagram ()
    {
      $instagram = new Instagram(4);
      return $instagram->images();
    }
  }
