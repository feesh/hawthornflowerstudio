<?php

  namespace app\controllers;

  use Bladerunner\Controller;
  use app\services\Helpers;

  class Base extends Controller
  {
    protected $id;
    protected $object;
    protected $hlp;
    protected $class;

    public function __construct () {
      $this->object = get_queried_object();
      $this->id = get_queried_object_id();
      $this->hlp = new Helpers;

      if ($this->splash() && !is_front_page()) {
        wp_redirect('/'); exit;
      }
    }

    public function navigation ()
    {
      return [
        'our-story' => get_page_link(6),
        'our-services' => get_page_link(8),
        'work' => get_page_link(107),
        'contact' => get_page_link(10),
        'gallery' => get_post_type_archive_link('gallery'),
        'blog' => get_page_link(get_option('page_for_posts')),
      ];
    }

    public function svg ()
    {
      return [
        'logo' => $this->hlp->pull_in_svg('logo'),
        'mark' => $this->hlp->pull_in_svg('mark'),
        'icon-instagram' => $this->hlp->pull_in_svg('icon-instagram'),
        'icon-pinterest' => $this->hlp->pull_in_svg('icon-pinterest')
      ];
    }

    public function social ()
    {
      return get_field('social', 'option');
    }

    public function credits ()
    {
      return get_field('credits', 'option');
    }

    public function splash ()
    {
      return false;
    }

    public function class ()
    {
      return $this->class;
    }
  }
