<?php
  namespace app\controllers;

  use app\controllers\Base;

  class Index extends Base
  {
    protected $template='views.pages.our-services';

    public function __construct ()
    {
      parent::__construct();
      $this->class = 'template:our-services';
    }

    public function image ()
    {
      return get_post_thumbnail_id($this->id);
    }

    public function col1 ()
    {
      return get_field('column_1_content', $this->id);
    }

    public function col2 ()
    {
      return get_field('column_2_content', $this->id);
    }

    public function col3 ()
    {
      return get_field('column_3_content', $this->id);
    }
  }
