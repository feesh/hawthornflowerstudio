<?php
  namespace app\controllers;

  use app\controllers\Base;
  use app\models\Gallery;

  class GalleryIndex extends Base
  {
    protected $template='views.gallery.index';
    private $limit = 10;
    private $page = 0;
    private $max = null;

    public function __construct ()
    {
      parent::__construct();
      $this->class = 'template:gallery';
      $this->max = ceil(count(Gallery::all())/$this->limit) - 1;
      $this->page = isset($_GET['page']) ? $_GET['page'] : 0;
    }

    public function items ()
    {
      return Gallery::limit($this->limit)->offset($this->page * $this->limit)->get()->map(function ($item) {
        $id = get_post_thumbnail_id($item->id);

        return [
          'image' => $id,
          'imagemeta' => wp_get_attachment_metadata($id),
          'heading' => $item->title,
          'location' => get_field('sub_heading', $item->id),
          'link' => $item->permalink()
        ];
      });
    }

    public function next ()
    {
      return $this->page < $this->max ? $this->page + 1 : false;
    }

    public function max ()
    {
      return $this->max;
    }
  }
