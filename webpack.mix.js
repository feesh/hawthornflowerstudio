const mix = require('laravel-mix')

const resources = 'app/resources'
const assets = 'content/themes/strt/assets'

mix.setPublicPath(assets)
mix.setResourceRoot('../')

mix.js(`${resources}/js/app.js`, `${assets}/js`)

mix.sass(`${resources}/scss/app.scss`, `${assets}/css`, {
  includePaths: ['node_modules']
})

mix.copy(`${resources}/fonts`, `${assets}/fonts`)
mix.copy(`${resources}/images`, `${assets}/images`)

// Hash and version files in production.
if (mix.config.inProduction) {
  mix.version()
}
