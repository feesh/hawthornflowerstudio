<?php

# include .env configuration
require_once(__DIR__ . '/vendor/autoload.php');
(new \Dotenv\Dotenv(__DIR__))->load();

$table_prefix  = getenv('DB_PREFIX');

define('APP_ROOT', __DIR__);
define('WP_CACHE', true);

define('DB_NAME', getenv('DB_NAME'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASSWORD', getenv('DB_PASSWORD'));
define('DB_HOST', getenv('DB_HOST'));

define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

define( 'WPMDB_LICENCE', getenv('WPMDB_LICENSE') );

define( 'ENV_TYPE', getenv('ENV_TYPE') );
define( 'SCRIPT_DEBUG', getenv('SCRIPT_DEBUG') );
define( 'SAVEQUERIES', getenv('SAVEQUERIES') );
define( 'WP_DEBUG', getenv('WP_DEBUG') );
define( 'WP_DEBUG_LOG', getenv('WP_DEBUG_LOG') );
define( 'WP_DEBUG_DISPLAY', getenv('WP_DEBUG_DISPLAY') );

define('WP_DEFAULT_THEME', getenv('WP_DEFAULT_THEME'));

define('AUTH_KEY',         ':+&{j|f[_X8[G91S)zvvJWc3~e?XEX`}^R-;O2fMl`Io+[#]6MtKt+W1+(v1?~48');
define('SECURE_AUTH_KEY',  'kiK-}q<7R);ly.uM0Kv$o~gCi?q|>|*0iD4hbdrDL$.LDUY<)t3sT)3TCfmB-@YT');
define('LOGGED_IN_KEY',    'iU_$](i8+cN%?,hBK4x-;6-5sP]9[gg|YCo.qb5O1s}T$mdyN6v]IkwSdMT7y#Fe');
define('NONCE_KEY',        '6Kt^A 4_04?A#ux}q4dW1$w %HEE.QI*&>(g*:hJ6Qx0ePU{9O3nQC(h6p?ee7}y');
define('AUTH_SALT',        '+P{oq=+2W63N0t8Wewe-bk,F[MI*B;n69DlnusNXV<8m9^<<?L-VT|@+PcbH2*f0');
define('SECURE_AUTH_SALT', 'Mi0ri%:)#+$k9)K]j BEilV49>X~QzW/O=jMjkvRHR[<kP8`roDXqnD9yn:5XN| ');
define('LOGGED_IN_SALT',   'i?g`Khu9W+lnSH&)|Ns@I&BJb(UjRX5f+xc7c(JoaeCur)Q,5RtiBatBYUyk>|!$');
define('NONCE_SALT',       '{33I2MfY--9-xzL1NJpI`e1`aRX)K_eB<ESN+W[p~2|@s=|i-QD*+7js0-r#]DO4');

$protocol = $_ENV['FORCE_SSL'] ? 'https://' : 'http://';
$server_name = str_replace('www.', '', $_SERVER['SERVER_NAME']);
$url = $protocol . $server_name;

define('WP_SITEURL', $url . '/cms');
define('WP_HOME', $url . '');
define('WP_CONTENT_DIR', dirname(__FILE__) . '/content');
define('WP_CONTENT_URL', $url . '/content');

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
