# STRT

Basic WP Starter

## Installation

1. Copy the `env.example` file and rename to `.env`
2. Edit the `THM` constant to the name of the theme
3. run `composer install`

## Theme Dependencies

1. `cd` into the theme
2. run `yarn install` or `npm install`

## Development
1. run `npm dev watch`


### SASS/CSS

This starter pack provides you with some start Mixins & Functions to help speed up development. Please read through the general descriptions below and then check the documentation.

#### Mixins

#### Functions

### JS
